<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TestAttempt extends Model
{
    protected $table = 'test_attempt';
    public $timestamps = false;
    protected $fillable = ['test_id', 'user_id', 'course_id'];

}
