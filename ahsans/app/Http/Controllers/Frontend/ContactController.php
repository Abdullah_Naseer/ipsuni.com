<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use Illuminate\Support\Facades\Mail;
use App\Mail\Frontend\Contact\SendContact;
use App\Http\Requests\Frontend\Contact\SendContactRequest;
use Illuminate\Support\Facades\Session;

/**
 * Class ContactController.
 */
class ContactController extends Controller
{

    private $path;

    public function __construct()
    {
        $path = 'frontend';
        if(session()->has('display_type')){
            if(session('display_type') == 'rtl'){
                $path = 'frontend-rtl';
            }else{
                $path = 'frontend';
            }
        }else if(config('app.display_type') == 'rtl'){
            $path = 'frontend-rtl';
        }
        $this->path = $path;
    }


    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view($this->path.'.contact');
    }

    /**
     * @param SendContactRequest $request
     *
     * @return mixed
     */
    public function send(SendContactRequest $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ]);
        $name=  $request->name;
        $phone=  $request->phone;
        $email=  $request->email;
        $message=  $request->message;
        if (strpos($name, 'porn') !== false || strpos($name, 'sex') !== false || strpos($name, 'adult') !== false || strpos($name, 'sexs') !== false || strpos($name, 'movie') !== false || strpos($name, 'movies') !== false || strpos($name, 'video') !== false || strpos($name, 'videos') !== false || strpos($name, 'girl') !== false || strpos($name, 'girls') !== false || strpos($name, 'dating') !== false || strpos($name, 'datings') !== false || strpos($name, 'beautiful') !== false || strpos($name, 'Sеху') !== false) {
           return redirect()->back()->with(['msg'=>'dafa dhur']);
        }
        if (strpos($phone, 'porn') !== false || strpos($phone, 'sex') !== false || strpos($phone, 'adult') !== false || strpos($phone, 'sexs') !== false || strpos($phone, 'movie') !== false || strpos($phone, 'movies') !== false || strpos($phone, 'video') !== false || strpos($phone, 'videos') !== false || strpos($phone, 'girl') !== false || strpos($phone, 'girls') !== false || strpos($phone, 'dating') !== false || strpos($phone, 'datings') !== false || strpos($phone, 'beautiful') !== false || strpos($phone, 'Sеху') !== false) {
           return redirect()->back()->with(['msg'=>'dafa dhur']);
        }
        if (strpos($email, 'porn') !== false || strpos($email, 'sex') !== false || strpos($email, 'adult') !== false || strpos($email, 'sexs') !== false || strpos($email, 'movie') !== false || strpos($email, 'movies') !== false || strpos($email, 'video') !== false || strpos($email, 'videos') !== false || strpos($email, 'girl') !== false || strpos($email, 'girls') !== false || strpos($email, 'dating') !== false || strpos($email, 'datings') !== false || strpos($email, 'beautiful') !== false || strpos($email, 'Sеху') !== false) {
           return redirect()->back()->with(['msg'=>'dafa dhur']);
        }
        if (strpos($message, 'porn') !== false || strpos($message, 'sex') !== false || strpos($message, 'adult') !== false || strpos($message, 'sexs') !== false || strpos($message, 'movie') !== false || strpos($message, 'movies') !== false || strpos($message, 'video') !== false || strpos($message, 'videos') !== false || strpos($message, 'girl') !== false || strpos($message, 'girls') !== false || strpos($message, 'dating') !== false || strpos($message, 'datings') !== false || strpos($message, 'beautiful') !== false || strpos($message, 'Sеху') !== false) {
           return redirect()->back()->with(['msg'=>'dafa dhur']);
        }

        $contact = new Contact();
        $contact->name = $name;
        $contact->number = $phone;
        $contact->email = $email;
        $contact->message = $message;
        $contact->save();
        

        Mail::send(new SendContact($request));

        Session::flash('alert','Contact mail sent successfully!');
        return redirect()->back();
    }
}
