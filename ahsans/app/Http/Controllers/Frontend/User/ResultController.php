<?php

namespace App\Http\Controllers\Frontend\User;

use App\Models\Course;
use App\Models\Question;
use App\Models\QuestionsOption;
use App\Models\Test;
use App\Models\TestAttempt;
use App\Models\TestsResult;
use App\Models\TestsResultsAnswer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class ResultController extends Controller
{
   public function index($id){
   $courseid = decrypt($id);


    $userId = Auth::id();
      $testAttempt=  TestAttempt::where(['user_id'=>$userId, 'course_id'=>$courseid])->first();

       if ($testAttempt == null) {

       if ($userId % 2 == 0) {
           $tests =  Test::with('questions.options')->where('course_id','=',$courseid)->first();
       }else{
           $tests =  Test::with('questions.options')->where('course_id','=',$courseid)->orderBy('id',"DESC")->first();
       }
       if ($tests != null){
           TestAttempt::create(['user_id'=>$userId, 'course_id'=>$courseid,'test_id' => $tests->id]);
           return view('backend.result.user-test',compact('tests'));
       }else{
           return Redirect::to('user/dashboard')->with(['message' =>'message']);
//           return redirect()->back()->with('message', 'IT WORKS!');
//           return redirect()->back()->with('errorTest', 'Oops! Sorry there is no question available in this course');
       }

       }else{
           return Redirect::to('quiz/result');
       }


   }
   public function addResult(Request $request){

     $correctOption =  QuestionsOption::where(['id'=>$request->optionId,'question_id'=>$request->questionId])->pluck('correct')->first();
     $checkOption =  TestsResultsAnswer::where(['question_id'=>$request->questionId,'user_id'=>Auth::id()])->first();
     $question =  Test::with('questions')->where('id','=',$request->testId)->first();

         $score= Question::where('id','=',$request->questionId)->pluck('score')->first();


     if($checkOption == null){
        $testResultId = TestsResult::create(['test_id'=>$request->testId,'test_result'=>$score,'user_id'=>Auth::id()]);

    TestsResultsAnswer::create([
             'tests_result_id'=>$testResultId->id,
             'option_id'=>$request->optionId,
             'question_id'=>$request->questionId,
             'correct'=>$correctOption,
             'user_id'=>Auth::id()
         ]);

       $number =  TestsResult::where(['test_id'=>$request->testId,'user_id'=>Auth::id()])->get();
        $percentage =  ((int)$number->sum('test_result') /(int)$question->questions->sum('score')) * 100 ;

        if (count($number) == count($question->questions)){
         if($percentage > 70){
             return response()->json(['success'=>'completed','result'=>'pass']);
         }else{
             return response()->json(['success'=>'completed','result'=>'fail']);
         }

        }else{
            return response()->json(['success'=>'success']);
        }

     }else{
         return response()->json(['error'=>'Already added']);
     }

   }
   public function getResult(){
      $user = Auth::user();
      $courseName = Course::where('id','=',$user->course_id)->pluck('title')->first();
      $test =  Test::with('questions')->where('course_id','=',$user->course_id)->first();
       $number =  TestsResult::where(['test_id'=>$test->id,'user_id'=>$user->id])->sum('test_result');
       $percentage =  ((int)$number /(int)$test->questions->sum('score')) * 100 ;

    return view('backend.result.result' ,compact('percentage','user','courseName'));
   }
}
