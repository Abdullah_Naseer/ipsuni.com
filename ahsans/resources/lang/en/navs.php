<?php

return array (
  'general' => 
  array (
    'logout' => 'Logout',
    'home' => 'Home',
    'login' => 'LogIn',
  ),
  'frontend' => 
  array (
    'contact' => 'Contact',
    'dashboard' => 'Dashboard',
    'login' => 'Login',
    'macros' => 'Macros',
    'register' => 'Register',
    'courses' => 'Courses',
    'forums' => 'Forums',
    'user' => 
    array (
      'account' => 'My Account',
      'quiz' => 'Questions',
      'administration' => 'Administration',
      'change_password' => 'Change Password',
      'my_information' => 'My Information',
      'profile' => 'Profile',
    ),
  ),
);
