@extends('backend.layouts.app')

@section('content')
    <link rel="stylesheet" href="{{url('public/css/quiz-style.css')}}">
    <div class="row justify-content-center align-items-center mb-3">
        <div class="col col-sm-12 align-self-center">
            <div class="card">
                <div class="card-header">
                    <h3 class="mb-0">@lang('navs.frontend.user.account')</h3>
                </div>

                <div class="card-body">

    <div class="row" style="margin-bottom: 20px;">
        <div class="col-md-4">
            <div class="course_name_wrapper bg-aqua">
                <a href="#" data-toggle="collapse" data-target="#demo">
                    <i class="icon-eye"></i>
                    @php
                        $string = getUserSubject();
                            $firstCharacter = $string[0];

                            $firstCharacter = substr($string, 0, 1);
                    @endphp
                    <span class="capital-alphabet">{{ $firstCharacter }}</span>
                    <div class="inner-wrapper">
                        <span>{{getUserSubject()}}</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="result_wrapper collapse" id="demo">
        <div class="result_wrapper_inner">
            <div class="points">
                @php
                    $result =  userResult($logged_in_user);
                @endphp
                {{$result}}%
            </div>
            @if($result > 70)
            <div class="view_certificate">
                {{--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>--}}
                <a type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#congrats-modal">View Certificate</a>
            </div>
                @endif
        </div>
    </div>
                </div>
                </div>
            </div>
        </div>


    <!-- Congrats Modal Start -->
    <div class="modal bounceIn" id="congrats-modal" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true">
        <canvas id="canvas" style="position: absolute"></canvas>
        <div class="modal-dialog modal-lg modal-dialog-centered quiz-container" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                <div class="modal-body p-0">
                    <div class="text-center">
                        <div class="congrats">
                            <div class="modal-top-bar">
                                <img width="100" height="100" src="{{url('public/img/quiz/congrats.gif')}}" alt="congrats">
                                <h1>Congratulations!</h1>
                                <img width="100" height="100" src="{{url('public/img/quiz/congrats.gif')}}" alt="congrats">
                            </div>
                            <div class="certicate-img-wrapper">

                                <img class="img-responsive" src="{{url('public/img/quiz/certificate.jpg')}}" width="100%" alt="certificate">
                                <span class="student-name" id="userName">{{$user->first_name}} {{$user->last_name}}</span>
                                <span class="course-name" id="courseId">{{$courseName}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Congrats Modal Start -->
    <div class="modal bounceIn" id="score-modal" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true">
        <canvas id="canvas" style="position: absolute"></canvas>
        <div class="modal-dialog modal-lg modal-dialog-centered quiz-container" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                <div class="modal-body p-0">
                    <div class="text-center">
                        <div class="congrats">
                            <div class="modal-top-bar">
                                <img width="100" height="100" src="{{url('public/img/quiz/congrats.gif')}}" alt="congrats">
                                <h1>Congratulations!</h1>
                                <img width="100" height="100" src="{{url('public/img/quiz/congrats.gif')}}" alt="congrats">
                            </div>
                            <div class="certicate-img-wrapper">
                                <h2>YOU GOT</h2>
                                <div class="marks-text">
                                    @php
                                        $result =  userResult($logged_in_user);
                                    @endphp
                                    {{$result}}%
                                </div>

                                {{--<img class="img-responsive" src="{{url('public/img/quiz/certificate.jpg')}}" width="100%" alt="certificate">--}}
                                {{--<span class="student-name" id="userName">{{$user->first_name}} {{$user->last_name}}</span>--}}
                                {{--<span class="course-name" id="courseId">{{$courseName}}</span>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Congrats Modal End -->
    <!-- Loser Modal Start -->
    <div class="modal bounceIn" id="loser-modal" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered quiz-container" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                <div class="modal-body p-0">
                    <div class="text-center">
                        <div class="loser">
                            <div class="modal-top-bar">
                                <img width="100" height="100" src="{{url('public/img/quiz/crying-icon.gif')}}" alt="">
                                <h1>Better Luck for next Time</h1>
                                <img width="100" height="100" src="{{url('public/img/quiz/crying-icon.gif')}}" alt="">
                            </div>
                            <img class="img-responsive" src="{{url('public/img/quiz/loser-img.jpg')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{url('public/js/quiz/jquery-2.2.4.min.js')}}"></script>
 @if($percentage > 70)
    <script>
        $(document).ready(function () {
            $('#score-modal').modal('show')

        })
    </script>
 @endif
 @if($percentage == 0)
     <script>
         $(document).ready(function () {
             $('#loser-modal').modal('show')
         })
     </script>
    @endif
 @if($percentage < 70)
     <script>
         $(document).ready(function () {
             $('#loser-modal').modal('show')
         })
     </script>
    @endif
@endsection
