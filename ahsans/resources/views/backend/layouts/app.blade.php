<!DOCTYPE html>
@langrtl
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @endlangrtl
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title', app_name())</title>
        <meta name="description" content="@yield('meta_description', 'Laravel 5 Boilerplate')">
        <meta name="author" content="@yield('meta_author', 'Anthony Rappa')">
        @if(config('favicon_image') != "")
            <link rel="shortcut icon" type="image/x-icon" href="{{asset('storage/logos/'.config('favicon_image'))}}"/>
        @endif
        @yield('meta')
        <link rel="stylesheet" href="{{asset('css/select2.min.css')}}">
        {{--<link rel="stylesheet" href="{{asset('css/animate.css')}}">--}}
        <link rel="stylesheet" href="{{asset('assets/css/fontawesome-all.css')}}">

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

        <link rel="stylesheet"
              href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css"/>
        <link rel="stylesheet"
              href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css"/>
        <link rel="stylesheet"
              href="//cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css"/>
        {{--<link rel="stylesheet"--}}
              {{--href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.standalone.min.css"/>--}}
        {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
        <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">


        @stack('before-styles')

    <!-- Check if the language is set to RTL, so apply the RTL layouts -->
        <!-- Otherwise apply the normal LTR layouts -->
        {{ style(asset('css/backend.css')) }}


        @stack('after-styles')
    </head>

    <body class="{{ config('backend.body_classes') }}">
    @include('backend.includes.header')

    <div class="app-body">
        @include('backend.includes.sidebar')

        <main class="main">
            @include('includes.partials.logged-in-as')
            {{--{!! Breadcrumbs::render() !!}--}}

            <div class="container-fluid" style="padding-top: 30px">
                <div class="animated fadeIn">
                    <div class="content-header">
                        @yield('page-header')
                    </div><!--content-header-->

                    @include('includes.partials.messages')
                    @yield('content')
                </div><!--animated-->
            </div><!--container-fluid-->
        </main><!--main-->

        @include('backend.includes.aside')
    </div><!--app-body-->

    @include('backend.includes.footer')

    <!-- Scripts -->
    @stack('before-scripts')
    {!! script(asset('js/manifest.js')) !!}
    {!! script(asset('js/vendor.js')) !!}
    {!! script(asset('js/backend.js')) !!}
    <script>
        //Route for message notification
        var messageNotificationRoute = '{{route('admin.messages.unread')}}'
    </script>
    <script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="{{asset('js/pdfmake.min.js')}}"></script>
    <script src="{{asset('js/vfs_fonts.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{url('public/js/quiz/jquery.smartWizard.js')}}"></script>
    <script src="{{url('public/js/quiz/wow.min.js')}}"></script>
    <script src="{{url('public/js/quiz/TweenMax.min.js')}}"></script>
    <script src="{{url('public/js/quiz/underscore-min.js')}}"></script>
    <script src="{{asset('js/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/main.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            //  Wizard 1
            $('#wizard1').smartWizard({
                transitionEffect:'fade',
                onFinish:onFinishCallback,
                onLeaveStep  : leaveAStepCallback,
            });
            function leaveAStepCallback(obj, context){
                // To check and enable finish button if needed
                if (context.fromStep >= 2) {
                    $('#wizard1').smartWizard('enableFinish', true);
                }
                return true;
            }
            //  Wizard 2
            $('#wizard2').smartWizard({transitionEffect:'slide',onFinish:onFinishCallback});
            function onFinishCallback(){
                alert('Finish Called');
            }
        });
    </script>
    <script>
        $(document).ready(function(){
            $(function() {
                var numberOfStars = 300;

                for (var i = 0; i < numberOfStars; i++) {
                    $('.congrats').append('<div class="blob fa fa-star ' + i + '"></div>');
                }

                animateText();

                animateBlobs();
            });

            $('.congrats').click(function() {
                reset();

                animateText();

                animateBlobs();
            });

            function reset() {
                $.each($('.blob'), function(i) {
                    TweenMax.set($(this), { x: 0, y: 0, opacity: 1 });
                });

                TweenMax.set($('h1'), { scale: 1, opacity: 1, rotation: 0 });
            }

            function animateText() {
                TweenMax.from($('h1'), 0.8, {
                    scale: 0.4,
                    opacity: 0,
                    rotation: 15,
                    ease: Back.easeOut.config(4),
                });
            }

            function animateBlobs() {

                var xSeed = _.random(500, 800);
                var ySeed = _.random(350, 400);

                $.each($('.blob'), function(i) {
                    var $blob = $(this);
                    var speed = _.random(1, 5);
                    var rotation = _.random(5, 100);
                    var scale = _.random(0.8, 1.5);
                    var x = _.random(-xSeed, xSeed);
                    var y = _.random(-ySeed, ySeed);

                    TweenMax.to($blob, speed, {
                        x: x,
                        y: y,
                        ease: Power1.easeOut,
                        opacity: 0,
                        rotation: rotation,
                        scale: scale,
                        onStartParams: [$blob],
                        onStart: function($element) {
                            $element.css('display', 'block');
                        },
                        onCompleteParams: [$blob],
                        onComplete: function($element) {
                            $element.css('display', 'none');
                        }
                    });
                });
            }

        });



        let W = window.innerWidth;
        let H = window.innerHeight;
        const canvas = document.getElementById("canvas");
        const context = canvas.getContext("2d");
        const maxConfettis = 150;
        const particles = [];

        const possibleColors = [
            "DodgerBlue",
            "OliveDrab",
            "Gold",
            "Pink",
            "SlateBlue",
            "LightBlue",
            "Gold",
            "Violet",
            "PaleGreen",
            "SteelBlue",
            "SandyBrown",
            "Chocolate",
            "Crimson"
        ];

        function randomFromTo(from, to) {
            return Math.floor(Math.random() * (to - from + 1) + from);
        }

        function confettiParticle() {
            this.x = Math.random() * W; // x
            this.y = Math.random() * H - H; // y
            this.r = randomFromTo(11, 33); // radius
            this.d = Math.random() * maxConfettis + 11;
            this.color =
                possibleColors[Math.floor(Math.random() * possibleColors.length)];
            this.tilt = Math.floor(Math.random() * 33) - 11;
            this.tiltAngleIncremental = Math.random() * 0.07 + 0.05;
            this.tiltAngle = 0;

            this.draw = function() {
                context.beginPath();
                context.lineWidth = this.r / 2;
                context.strokeStyle = this.color;
                context.moveTo(this.x + this.tilt + this.r / 3, this.y);
                context.lineTo(this.x + this.tilt, this.y + this.tilt + this.r / 5);
                return context.stroke();
            };
        }

        function Draw() {
            const results = [];

// Magical recursive functional love
            requestAnimationFrame(Draw);

            context.clearRect(0, 0, W, window.innerHeight);

            for (var i = 0; i < maxConfettis; i++) {
                results.push(particles[i].draw());
            }

            let particle = {};
            let remainingFlakes = 0;
            for (var i = 0; i < maxConfettis; i++) {
                particle = particles[i];

                particle.tiltAngle += particle.tiltAngleIncremental;
                particle.y += (Math.cos(particle.d) + 3 + particle.r / 2) / 2;
                particle.tilt = Math.sin(particle.tiltAngle - i / 3) * 15;

                if (particle.y <= H) remainingFlakes++;

                // If a confetti has fluttered out of view,
                // bring it back to above the viewport and let if re-fall.
                if (particle.x > W + 30 || particle.x < -30 || particle.y > H) {
                    particle.x = Math.random() * W;
                    particle.y = -30;
                    particle.tilt = Math.floor(Math.random() * 10) - 20;
                }
            }

            return results;
        }

        window.addEventListener(
            "resize",
            function() {
                W = window.innerWidth;
                H = window.innerHeight;
                canvas.width = window.innerWidth;
                canvas.height = window.innerHeight;
            },
            false
        );

        // Push new confetti objects to `particles[]`
        for (var i = 0; i < maxConfettis; i++) {
            particles.push(new confettiParticle());
        }

        // Initialize
        canvas.width = W;
        canvas.height = H;
        Draw();


    </script>
    <script>
        window._token = '{{ csrf_token() }}';
    </script>
    @stack('after-scripts')

    <script>
        $(document).ready(function () {

            $("#quizButton").click(function(){
                $.ajax({
                    method: 'POST',
                    url: '{{url('quiz/submit')}}',
                    dataType:'json',
                    data: {
                        _token: _token,
                        questionId: $('#questionId').val(),
                        optionId: $('#optionId').val(),
                        testId: $('#testId').val(),
                    }
                }).done(function (data) {

                   if(data.success == 'completed'){
                           location.reload();

                   }
                });
            });
        })
    </script>
    <script>
        $(document).ready(function () {
            setTimeout(function() {
                $('#mydiv').fadeOut('fast');
            }, 1000)
        });

    </script>
    </body>
    </html>
