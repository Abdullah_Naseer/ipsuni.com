<!DOCTYPE html>
@langrtl
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @endlangrtl
    <head>
        <style>
        /*Sticky social icons*/
.sticky-container{
    padding: 0px;
    margin: 0px;
    position: fixed;
    top: 40%;
    width: 200px;
    z-index: 100000;
    left: -158px;
	}

	.sticky li{
		list-style-type: none;
		background-color: #333;
		color: #efefef;
		height: 43px;
		padding: 0px;
		margin: 0px 0px 1px 0px;
		-webkit-transition:all 0.25s ease-in-out;
		-moz-transition:all 0.25s ease-in-out;
		-o-transition:all 0.25s ease-in-out;
		transition:all 0.25s ease-in-out;
		cursor: pointer;
		filter: url("data:image/svg+xml;utf8,<svg xmlns=\'http://www.w3.org/2000/svg\'><filter id=\'grayscale\'><feColorMatrix type=\'matrix\' values=\'0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0\'/></filter></svg>#grayscale");
                filter: gray;
                -webkit-filter: grayscale(100%);

	}

	.sticky li:hover{
		margin-right: -20px;
		/*-webkit-transform: translateX(-115px);
		-moz-transform: translateX(-115px);
		-o-transform: translateX(-115px);
		-ms-transform: translateX(-115px);
		transform:translateX(-115px);*/
		/*background-color: #8e44ad;*/
		filter: url("data:image/svg+xml;utf8,<svg xmlns=\'http://www.w3.org/2000/svg\'><filter id=\'grayscale\'><feColorMatrix type=\'matrix\' values=\'1 0 0 0 0, 0 1 0 0 0, 0 0 1 0 0, 0 0 0 1 0\'/></filter></svg>#grayscale");
                -webkit-filter: grayscale(0%);
	}

	.sticky li img{
		float: right;
		margin: 5px 5px;


	}

	.sticky li p{
		padding: 0px;
		margin: 0px;
		text-transform: uppercase;
		line-height: 43px;
		float:right;
		margin-right:5px;

	}


/*Sticky social icons*/
        </style>
        <meta name="google-site-verification" content="la1RIPeUII8EiwNCd6tiMTiaKY8LT1tMARj5RoqyCgI" />
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @if(config('favicon_image') != "")
            <link rel="shortcut icon" type="image/x-icon" href="{{asset('storage/logos/'.config('favicon_image'))}}"/>
        @endif
        <title>@yield('title', app_name())</title>
        <meta name="description" content="@yield('meta_description','')">
        <meta name="keywords" content="@yield('meta_keywords', 'IPS Uni, IT Institute, Short Courses in Lahore, ')">

        {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
        @stack('before-styles')

    <!-- Check if the language is set to RTL, so apply the RTL layouts -->
        <!-- Otherwise apply the normal LTR layouts -->

        <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/flaticon.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/meanmenu.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/video.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/lightbox.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/progess.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/animate.min.css')}}">
        {{--<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">--}}
        <link rel="stylesheet" href="{{ asset('css/frontend.css') }}">
        <link rel="stylesheet" href="{{asset('assets/css/fontawesome-all.css')}}">

        <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">

        <link rel="stylesheet" href="{{asset('assets/css/colors/switch.css')}}">
        <link href="{{asset('assets/css/colors/color-2.css')}}" rel="alternate stylesheet" type="text/css"
              title="color-2">
        <link href="{{asset('assets/css/colors/color-3.css')}}" rel="alternate stylesheet" type="text/css"
              title="color-3">
        <link href="{{asset('assets/css/colors/color-4.css')}}" rel="alternate stylesheet" type="text/css"
              title="color-4">
        <link href="{{asset('assets/css/colors/color-5.css')}}" rel="alternate stylesheet" type="text/css"
              title="color-5">
        <link href="{{asset('assets/css/colors/color-6.css')}}" rel="alternate stylesheet" type="text/css"
              title="color-6">
        <link href="{{asset('assets/css/colors/color-7.css')}}" rel="alternate stylesheet" type="text/css"
              title="color-7">
        <link href="{{asset('assets/css/colors/color-8.css')}}" rel="alternate stylesheet" type="text/css"
              title="color-8">
        <link href="{{asset('assets/css/colors/color-9.css')}}" rel="alternate stylesheet" type="text/css"
              title="color-9">
        <!--<script src="https://www.google.com/recaptcha/api.js?render=6LcHIcoUAAAAAKACJBZasyL7meQbRn01DP3ZJO7p"></script>-->
          <!--<script>
        grecaptcha.ready(function() {
            grecaptcha.execute('6LcHIcoUAAAAAKACJBZasyL7meQbRn01DP3ZJO7p', {action: 'contact'});
        });

        </script>-->
        
        @yield('css')
        @stack('after-styles')

        @if(config('google_analytics_id') != "")
    <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id={{config('google_analytics_id')}}"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', '{{config('google_analytics_id')}}');
        </script>
            @endif


    </head>
    <body class="{{config('layout_type')}}">
        


        
       <!--sticky socail icons-->
<div class="sticky-container hidden-xs">
   <ul class="sticky">
      <li>
          <a href="https://www.facebook.com/IPS-UNI-440939226724316" target="_blank">
              <img width="32" height="32" title="" alt="facebook" src="{{url('public/storage/uploads/facebook.png')}}">
          </a>
      </li>
      <li>
          <a href="https://twitter.com/IpsUni" target="_blank">
              <img width="32" height="32" title="" alt="twitter" src="{{url('public/storage/uploads/twitter.png')}}">
          </a>
      </li>
      <li>
          <a href="https://www.linkedin.com/company/ips-uni" target="_blank">
              <img width="32" height="32" title="" alt="LinkedIn" src="{{url('public/storage/uploads/in.png')}}">
          </a>
      </li>
      <li>
          <a href="https://www.instagram.com/ipsuniedu" target="_blank">
              <img width="32" height="32" title="" alt="instagram" src="{{url('public/storage/uploads/instagram logo.png')}}">
          </a>
      </li>
      <li>
          <a href="https://www.youtube.com/channel/UCzd7apOW0BGlDCMTegJ7Gnw?sub_confirmation=1" target="_blank">
              <img width="32" height="32" title="" alt="youtube" src="{{url('public/storage/uploads/youtube logo.png')}}">
          </a>
      </li>
   </ul>
</div>
<!--sticky socail icons-->
       
    @include('frontend.layouts.modals.loginModal')

    <div id="app">
    {{--<div id="preloader"></div>--}}


    <!-- Start of Header section
        ============================================= -->
        <header>
            <div id="main-menu" class="main-menu-container">
                <div class="main-menu">
                    <div class="container">
                        <div class="navbar-default">
                            <div class="navbar-header float-left">
                                <a class="navbar-brand text-uppercase" href="{{url('/')}}">
                                    {{--<img src="{{asset("storage/logos/".config('logo_w_image'))}}" alt="logo">--}}
                                    <img src="{{asset("storage/logos/".config('logo_w_image'))}}" alt="logo">
                                </a>
                            </div><!-- /.navbar-header -->

                            <div class="cart-search float-right ul-li">
                                <ul>
                                    <li>
                                        <a href="{{route('cart.index')}}"><i class="fas fa-shopping-bag"></i>
                                            @if(auth()->check() && Cart::session(auth()->user()->id)->getTotalQuantity() != 0)
                                                <span class="badge badge-danger position-absolute">{{Cart::session(auth()->user()->id)->getTotalQuantity()}}</span>
                                            @endif
                                        </a>
                                    </li>
                                </ul>
                            </div>


                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <nav class="navbar-menu float-right">
                                <div class="nav-menu ul-li">

                                    <ul>
                                        @if(count($custom_menus) > 0 )
                                            @foreach($custom_menus as $menu)
                                                @if(is_array($menu['id']) && $menu['id'] == $menu['parent'])
                                                    @if($menu->subs && (count($menu->subs) > 0))
                                                        <li class="menu-item-has-children ul-li-block">
                                                            <a href="#!">{{$menu->label}}</a>
                                                            <ul class="sub-menu">
                                                                @foreach($menu->subs as $item)
                                                                    @include('frontend.layouts.partials.dropdown', $item)
                                                                @endforeach
                                                            </ul>
                                                        </li>
                                                    @endif
                                                @else
                                                    <li class="nav-item">
                                                        <a href="{{url($menu->link)}}"
                                                           class="nav-link {{ active_class(Active::checkRoute('frontend.user.dashboard')) }}"
                                                           id="menu-{{$menu->id}}">{{ $menu->label }}</a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        @endif

                                        @if(auth()->check())
                                            <li class="menu-item-has-children ul-li-block">
                                                <a href="#!">{{ $logged_in_user->name }}</a>
                                                <ul class="sub-menu">
                                                    @can('view backend')
                                                        <li>
                                                            <a href="{{ route('admin.dashboard') }}">@lang('navs.frontend.dashboard')</a>
                                                        </li>
                                                    @endcan

                                                    <li>
                                                        <a href="{{ route('frontend.auth.logout') }}">@lang('navs.general.logout')</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        @else
                                            <li>
                                                <div class="log-in mt-0">
                                                    <a id="openLoginModal" data-target="#myModal"
                                                       href="#">@lang('navs.general.login')</a>
                                                    <!-- The Modal -->
                                                    @include('frontend.layouts.modals.loginModal')
                                                </div>
                                            </li>
                                        @endif
                                            @if(count($locales) > 1)
                                            <li class="menu-item-has-children ul-li-block">
                                                <a href="#">
                                                    <span class="d-md-down-none">@lang('menus.language-picker.language')
                                                        ({{ strtoupper(app()->getLocale()) }})</span>
                                                </a>
                                                <ul class="sub-menu">
                                                    @foreach($locales as $lang)
                                                        @if($lang != app()->getLocale())
                                                            <li>
                                                                <a href="{{ '/lang/'.$lang }}"
                                                                   class=""> @lang('menus.language-picker.langs.'.$lang)</a>
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </nav>

                            <div class="mobile-menu">
                                <div class="logo">
                                    <a href="{{url('/')}}">
                                        <img src={{asset("storage/logos/".config('logo_w_image'))}} alt="Logo">
                                    </a>
                                </div>
                                <nav>
                                    <ul>
                                        <!--@if(count($custom_menus) > 0 )-->
                                        <!--    @foreach($custom_menus as $menu)-->
                                        <!--        @if($menu['id'] == $menu['parent'])-->
                                        <!--            @if(count($menu->subs) > 0)-->
                                        <!--                <li class="">-->
                                        <!--                    <a href="#!">{{$menu->label}}</a>-->
                                        <!--                    <ul class="">-->
                                        <!--                        @foreach($menu->subs as $item)-->
                                        <!--                            @include('frontend.layouts.partials.dropdown', $item)-->
                                        <!--                        @endforeach-->
                                        <!--                    </ul>-->
                                        <!--                </li>-->
                                        <!--            @endif-->
                                        <!--        @else-->
                                        <!--            <li class="">-->
                                        <!--                <a href="{{url($menu->link)}}"-->
                                        <!--                   class="nav-link {{ active_class(Active::checkRoute('frontend.user.dashboard')) }}"-->
                                        <!--                   id="menu-{{$menu->id}}">{{ $menu->label }}</a>-->
                                        <!--            </li>-->
                                        <!--        @endif-->
                                        <!--    @endforeach-->
                                        <!--@endif-->
                                         @if(count($custom_menus) > 0 )
                                            @foreach($custom_menus as $menu)
                                                @if(is_array($menu['id']) && $menu['id'] == $menu['parent'])
                                                    @if($menu->subs && (count($menu->subs) > 0))
                                                        <li class="menu-item-has-children ul-li-block">
                                                            <a href="#!">{{$menu->label}}</a>
                                                            <ul class="sub-menu">
                                                                @foreach($menu->subs as $item)
                                                                    @include('frontend.layouts.partials.dropdown', $item)
                                                                @endforeach
                                                            </ul>
                                                        </li>
                                                    @endif
                                                @else
                                                    <li class="nav-item">
                                                        <a href="{{url($menu->link)}}"
                                                           class="nav-link {{ active_class(Active::checkRoute('frontend.user.dashboard')) }}"
                                                           id="menu-{{$menu->id}}">{{ $menu->label }}</a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        @endif
                                        @if(auth()->check())
                                            <li class="">
                                                <a href="#!">{{ $logged_in_user->name}}</a>
                                                <ul class="">
                                                    @can('view backend')
                                                        <li>
                                                            <a href="{{ route('admin.dashboard') }}">@lang('navs.frontend.dashboard')</a>
                                                        </li>
                                                    @endcan


                                                    <li>
                                                        <a href="{{ route('frontend.auth.logout') }}">@lang('navs.general.logout')</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        @else
                                            <li>
                                                <div class=" ">
                                                    <a id="openLoginModal" data-target="#myModal"
                                                       href="#">@lang('navs.general.login')</a>
                                                    <!-- The Modal -->
                                                </div>
                                            </li>
                                        @endif
                                            @if(count($locales) > 1)
                                                <li class="menu-item-has-children ul-li-block">
                                                    <a href="#">
                                                    <span class="d-md-down-none">@lang('menus.language-picker.language')
                                                        ({{ strtoupper(app()->getLocale()) }})</span>
                                                    </a>
                                                    <ul class="">
                                                        @foreach($locales as $lang)
                                                            @if($lang != app()->getLocale())
                                                                <li>
                                                                    <a href="{{ '/lang/'.$lang }}"
                                                                       class=""> @lang('menus.language-picker.langs.'.$lang)</a>
                                                                </li>
                                                            @endif
                                                        @endforeach
                                                    </ul>
                                                </li>
                                            @endif
                                    </ul>
                                </nav>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Start of Header section
            ============================================= -->


        @yield('content')

    <!-- Load Facebook SDK for JavaScript -->
<!-- <div id="fb-root"></div>-->
<!--<script>-->
<!--  window.fbAsyncInit = function() {-->
<!--    FB.init({-->
<!--      xfbml            : true,-->
<!--      version          : 'v3.3'-->
<!--    });-->
<!--  };-->

<!--  (function(d, s, id) {-->
<!--  var js, fjs = d.getElementsByTagName(s)[0];-->
<!--  if (d.getElementById(id)) return;-->
<!--  js = d.createElement(s); js.id = id;-->
<!--  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';-->
<!--  fjs.parentNode.insertBefore(js, fjs);-->
<!--}(document, 'script', 'facebook-jssdk'));</script>-->

<!-- Your customer chat code -->
<!--<div class="fb-customerchat"-->
<!--  attribution=setup_tool-->
<!--  page_id="440939226724316">-->
<!--</div>-->



        @include('frontend.layouts.partials.footer')

    </div><!-- #app -->




    <!-- Scripts -->
    @stack('before-scripts')

    <!-- For Js Library -->
    <script src="{{asset('assets/js/jquery-2.1.4.min.js')}}"></script>
    <script src="{{asset('assets/js/popper.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('assets/js/jarallax.js')}}"></script>
    <script src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('assets/js/lightbox.js')}}"></script>
    <script src="{{asset('assets/js/jquery.meanmenu.js')}}"></script>
    <script src="{{asset('assets/js/scrollreveal.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('assets/js/waypoints.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery-ui.js')}}"></script>
    <script src="{{asset('assets/js/contact.js')}}"></script>
    <script src="{{asset('assets/js/validator.js')}}"></script>
    <script src="{{asset('assets/js/gmap3.min.js')}}"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="{{asset('assets/js/switch.js')}}"></script>

<!-- GetButton.io widget -->
<script type="text/javascript">
(function () {
var options = {
facebook: "440939226724316", // Facebook page ID
whatsapp: "+92(309)777-7011", // WhatsApp number
call_to_action: "Message us", // Call to action
button_color: "#FF6550", // Color of button
position: "right", // Position may be 'right' or 'left'
order: "facebook,whatsapp", // Order of buttons
};
var proto = document.location.protocol, host = "getbutton.io", url = proto + "//static." + host;
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
})();
</script>
<!-- /GetButton.io widget -->


<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5e87172569e9320caabfe205/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->


    <script src="{{asset('assets/js/script.js')}}"></script>
    <script>
        @if((session()->has('show_login')) && (session('show_login') == true))
        $('#myModal').modal('show');
                @endif
        var font_color = "{{config('font_color')}}"
        setActiveStyleSheet(font_color);
    </script>

    @yield('js')

    @stack('after-scripts')

    @include('includes.partials.ga')
    </body>
    </html>
