@extends('backend.layouts.app')

@section('content')
    <div class="row justify-content-center align-items-center mb-3">
        <div class="col col-sm-12 align-self-center">
            <div class="card">
                <div class="card-header">
                    <h3 class="mb-0">@lang('navs.frontend.user.account')</h3>
                </div>

                <div class="card-body">
                    <div role="tabpanel">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a href="#profile" class="nav-link active" aria-controls="profile" role="tab" data-toggle="tab">@lang('navs.frontend.user.profile')</a>
                            </li>

                            <li class="nav-item">
                                <a href="#edit" class="nav-link" aria-controls="edit" role="tab" data-toggle="tab">@lang('labels.frontend.user.profile.update_information')</a>
                            </li>

                            @if($logged_in_user->canChangePassword())
                                <li class="nav-item">
                                    <a href="#password" class="nav-link" aria-controls="password" role="tab" data-toggle="tab">@lang('navs.frontend.user.change_password')</a>
                                </li>
                            @endif
                            <li class="nav-item">
                                    <a href="#result" class="nav-link" aria-controls="result" role="tab" data-toggle="tab">Result</a>
                                </li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade show active pt-3" id="profile" aria-labelledby="profile-tab">
                                @include('backend.account.tabs.profile')
                            </div><!--tab panel profile-->

                            <div role="tabpanel" class="tab-pane fade show pt-3" id="edit" aria-labelledby="edit-tab">
                                @include('backend.account.tabs.edit')
                            </div><!--tab panel profile-->

                            @if($logged_in_user->canChangePassword())
                                <div role="tabpanel" class="tab-pane fade show pt-3" id="password" aria-labelledby="password-tab">
                                    @include('backend.account.tabs.change-password')
                                </div><!--tab panel change password-->
                            @endif
                            <div role="tabpanel" class="tab-pane fade show pt-3" id="result" aria-labelledby="result-tab">
                                <div class="row" style="margin-bottom: 20px;">
                                    <div class="col-md-4">
                                        <div class="course_name_wrapper bg-aqua">
                                            <a href="#" data-toggle="collapse" data-target="#demo">
                                                <i class="icon-eye"></i>
                                                @php
                                                $string = getUserSubject();
                                                    $firstCharacter = $string[0];

                                                    $firstCharacter = substr($string, 0, 1);
                                                    @endphp
                                                <span class="capital-alphabet">{{ $firstCharacter }}</span>
                                                <div class="inner-wrapper">
                                                    <span>{{getUserSubject()}}</span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="result_wrapper collapse" id="demo">
                                    <div class="result_wrapper_inner">
                                        <div class="points">
                                            @php
                                            $result =  userResult($logged_in_user);
                                            @endphp
                                            {{$result}}%
                                        </div>
                                        <div class="view_certificate">
                                            <a href="#">View Certificate</a>
                                        </div>
                                    </div>
                                </div>

                                </div><!--tab panel change password-->

                        </div><!--tab content-->
                    </div><!--tab panel-->
                </div><!--card body-->
            </div><!-- card -->
        </div><!-- col-xs-12 -->
    </div><!-- row -->
    <link rel="stylesheet" href="{{url('public/css/quiz-style.css')}}">

    <!-- Congrats Modal Start -->
    <div class="modal bounceIn" id="congrats-modal" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true">
        <canvas id="canvas" style="position: absolute"></canvas>
        <div class="modal-dialog modal-lg modal-dialog-centered quiz-container" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                <div class="modal-body p-0">
                    <div class="text-center">
                        <div class="congrats">
                            <div class="modal-top-bar">
                                <img width="100" height="100" src="{{url('public/img/quiz/congrats.gif')}}" alt="congrats">
                                <h1>Congratulations!</h1>
                                <img width="100" height="100" src="{{url('public/img/quiz/congrats.gif')}}" alt="congrats">
                            </div>
                            <div class="certicate-img-wrapper">
                                <img class="img-responsive" src="{{url('public/img/quiz/certificate.jpg')}}" width="100%" alt="certificate">
                                <span class="student-name" id="userName">{{$logged_in_user->first_name}} {{$logged_in_user->last_name}}</span>
                                <span class="course-name" id="courseId"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Congrats Modal End -->
    <!-- Loser Modal Start -->
    <div class="modal bounceIn" id="loser-modal" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered quiz-container" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                <div class="modal-body p-0">
                    <div class="text-center">
                        <div class="loser">
                            <div class="modal-top-bar">
                                <img width="100" height="100" src="{{url('public/img/quiz/crying-icon.gif')}}" alt="">
                                <h1>Better Luck for next Time</h1>
                                <img width="100" height="100" src="{{url('public/img/quiz/crying-icon.gif')}}" alt="">
                            </div>
                            <img class="img-responsive" src="{{url('public/img/quiz/loser-img.jpg')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{url('public/js/quiz/jquery-2.2.4.min.js')}}"></script>
        <script>
        function modelFunction(no) {

        }
        </script>

@endsection
