@extends('backend.layouts.app')

@section('content')
    <link rel="stylesheet" href="{{url('public/css/quiz-style.css')}}">
    <div class="row justify-content-center align-items-center quiz-container">
        <div class="col col-sm-12 align-self-center">
            <div class="card">
                <div class="card-header">
                    <h3 class="mb-0">@lang('navs.frontend.user.quiz')</h3>
                </div>

                <div class="card-body">
                    <div class="main-box-wrapper">
                        <div class="inner-wrapper">
                            <div class="mb-4">
                                <center><img width="300" class="logo" src="{{url('public/img/quiz/ipsuni-logo.png')}}"></center>
                            </div>
                            <div class="quizContainer container-fluid well well-lg">
                                {{--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#congrats-modal">--}}
                                    {{--Congratulation--}}
                                {{--</button>--}}
                                {{--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#loser-modal">--}}
                                    {{--Loser--}}
                                {{--</button>--}}
                                <div id="quiz1" class="text-center">
                                    <h2 class="text-blue heading"></h2>
                                    <h4 class="timer"><img width="100" height="100" src="{{url('public/img/quiz/clock.gif')}}" alt=""> <span id="iTimeShow">Time Remaining: </span><span id='timer' style="font-size:20px;"></span></h4>
                                </div>
                                <form action="">
                                    <div id="wizard1" class="swMain">
                                       @if($tests)
                                           @php
                                           $count = 1;
                                           $counter = 1;

                                           @endphp
                                            <input type="hidden" id="testId" value="{{$tests->id}}">
                                            <input type="hidden" id="questionId" >
                                            <input type="hidden" id="optionId" >
                                       @foreach($tests->questions as $questions)
                                        <div id="step-{{$count}}">
                                            <div class="question">{{$count}}. {{$questions->question}}?</div>
                                            <ul class="choiceList">

                                                @foreach($questions->options as $option)
                                                <li><input type="radio" id="{{$counter}}" class="radio-inline" value="{{$option->id}}" onclick="valueSet('{{$questions->id}}','{{$option->id}}',)" name="dynradio"><label for="{{$counter}}"> {{$option->option_text}} </label></li>
                                                    @php
                                                        $counter ++;
                                                    @endphp
                                                @endforeach
                                            </ul>
                                        </div>
                                                @php
                                                    $count ++;
                                                @endphp
                                           @endforeach
                                           @endif

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div><!--card body-->
            </div><!-- card -->
        </div><!-- col-xs-12 -->
    </div><!-- row -->


    <script src="{{url('public/js/quiz/jquery-2.2.4.min.js')}}"></script>
    <div class="modal " id="alert-modal" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered " role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                <div class="modal-body p-0">
                    <h1 class="alert-danger" style="text-align: center"> Attention Students!</h1>
                    <p style="font-size: 22px; padding: 20px 40px">
                        Please don’t hit the refresh button, otherwise, you will lose your attempt to appear in this exam. Keep the page as it is, and continue to answer questions of your choice. We will calculate your final score based on the number of questions you attempt without any negative marking.
                    </p>

                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#alert-modal').modal('show')
        })
    </script>
    <script>
    function valueSet(questioId,optionId) {

        $('#questionId').val(questioId);
        $('#optionId').val(optionId);
    }

</script>

    <!-- timer -->
    <script>
        function countdown(element, minutes, seconds) {
            // set time for the particular countdown
            var time = minutes*60 + seconds;
            var interval = setInterval(function() {
                var el = document.getElementById(element);
                // if the time is 0 then end the counter
                if (time <= 0) {
                    var text = "hello";
                    location.reload();
                    setTimeout(function() {
                        countdown('timer', 2, 5);
                    }, 2000);
                    clearInterval(interval);
                    return;
                }
                var minutes = Math.floor( time / 60 );
                if (minutes < 10) minutes = "0" + minutes;
                var seconds = time % 60;
                if (seconds < 10) seconds = "0" + seconds;
                var text = minutes + ':' + seconds;
                el.innerHTML = text;
                time--;
            }, 1000);
        }
        countdown('timer', 2, 5);
    </script>
    <!-- timer -->
@endsection
