<?php
session_start();
$data = $_SESSION['data'];

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>P3Care - Index</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.png"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!--===============================================================================================-->
</head>
<body>
<div class="limiter">
    <div class="container-login100" style="background-image: url('images/bg-cover.jpg');">
        <div class="logo">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="logo-img">
                            <a href="#">
                                <img src="images/p3care-logo.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="myTabContent">
                        <div class="tab-pane fade show active" id="rcm" role="tabpanel" aria-labelledby="rcm-tab">
                            <form class="login100-form validate-form flex-sb flex-w" method="post" action="cdata.php">
                                <div style="width: 100%;" class="login100-form validate-form flex-sb flex-w">
                                    <div class="row">

                                        <div class="col-md-5">
                                            <div class="p-t-31 p-b-9">
                                          <span class="txt1">
                                          NPI Number
                                          </span>
                                            </div>
                                            <div class="wrap-input100 validate-input">
                                                <input class="input100" type="text" placeholder="Npi Number" id="npi" name="npi" value="1609979822">
                                                <span class="focus-input100"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="p-t-31 p-b-9">
                                          <span class="txt1">
                                          Year
                                          </span>
                                            </div>
                                            <div class="wrap-input100 validate-input">
                                                <input class="input100" type="text" placeholder="Npi Number" id="year" name="year" value="2019" >
                                                <span class="focus-input100"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <label class="p-t-31 p-b-9" for="">&nbsp; </label>
                                            <!--                                            <button class="login100-form-btn"  id="test" onclick="testFunction()" >-->
                                            <button class="login100-form-btn"  id="test" type="submit" >
                                                Get Record
                                            </button>
                                        </div>


                                        <div class="col-md-12 mt-4">
                                            <div class="user-table">

                                                <table class="table table-hover mb-0">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">First Name</th>
                                                        <th scope="col">Last Name</th>
                                                        <th scope="col">First Approval Date</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="myTable">

                                                    <tr>
                                                        <td id="fname"><?php echo  $data->data->clinician->first_name ?></td>
                                                        <td id="fname"><?php echo  $data->data->clinician->last_name ?></td>
                                                        <td id="fname"><?php echo  $data->data->clinician->first_approval_date ?></td>

                                                    </tr>
                                                    <tr>
                                                        <th scope="col" >Is Low Volume</th>
                                                        <th scope="col" >Organization Name</th>
                                                        <th scope="col" >Address</th>
                                                    </tr>

                                                    <?php
                                                    for ($a=0;$a<count($data->data->clinician->practiceClinicianStatuses->nodes); $a++ ){

                                                        ?>
                                                        <tr>
                                                            <td>
                                                                <?php if($data->data->clinician->practiceClinicianStatuses->nodes[$a]->is_low_volume){
                                                                    echo "true";
                                                                } else{
                                                                    echo "false";
                                                                }   ?>
                                                            </td>
                                                            <td>
                                                                <?php echo $data->data->clinician->practiceClinicianStatuses->nodes[$a]->practice->org_name  ; ?>
                                                            </td>
                                                               <td>
                                                                <?php
                                                                echo $data->data->clinician->practiceClinicianStatuses->nodes[$a]->practice->address->line_1   ;echo "&nbsp";
                                                                echo $data->data->clinician->practiceClinicianStatuses->nodes[$a]->practice->address->line_2   ;echo "&nbsp";
                                                                echo $data->data->clinician->practiceClinicianStatuses->nodes[$a]->practice->address->city   ;echo "&nbsp";
                                                                echo $data->data->clinician->practiceClinicianStatuses->nodes[$a]->practice->address->state   ;echo "&nbsp";
                                                                echo $data->data->clinician->practiceClinicianStatuses->nodes[$a]->practice->address->zip_code   ;
                                                                ?>
                                                            </td>
                                                        <td>
                                                            <?php
                                                            for($b=0;$b<count($data->data->clinician->practiceClinicianStatuses->nodes[$a]->practice->practiceStatuses->nodes);$b++){
                                                            ?>
                                                                <tr>
                                                                    <th scope="col" >Is Aci Reweighted</th>
                                                            <td>
                                                                <?php
                                                                if($data->data->clinician->practiceClinicianStatuses->nodes[$a]->practice->practiceStatuses->nodes[$b]->is_aci_reweighted)
                                                                {
                                                                    echo "true";
                                                                } else{
                                                                    echo "false";
                                                                }
                                                                ?>
                                                            </td>

                                                                    <th scope="col" >Is Hospital Based</th>
                                                                    <td>
                                                                <?php
                                                                if($data->data->clinician->practiceClinicianStatuses->nodes[$a]->practice->practiceStatuses->nodes[$b]->is_hospital_based)
                                                                {
                                                                    echo "true";
                                                                } else{
                                                                    echo "false";
                                                                }
                                                                ?>
                                                            </td>
                                                                </tr>
                                                            <?php
                                                            }
                                                            ?>
                                                        </td>

                                                        </tr>
                                                        <?php
                                                    }
                                                    ?>



                                                    </tbody>
                                                </table>
                                                <?php
                                                //
                                                //print_r($data->data->clinician)
                                                //?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="dropDownSelect1"></div>
<!--===============================================================================================-->
<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/bootstrap/js/popper.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/daterangepicker/moment.min.js"></script>
<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="js/main.js"></script>

</body>
</html>

