<!DOCTYPE html>
<html lang="en">
<head>
    <title>P3Care - Index</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.png"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!--===============================================================================================-->
</head>
<body>
<div class="limiter">
    <div class="container-login100" style="background-image: url('images/bg-cover.jpg');">
        <div class="logo">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="logo-img">
                            <a href="#">
                                <img src="images/p3care-logo.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="myTabContent">
                        <div class="tab-pane fade show active" id="rcm" role="tabpanel" aria-labelledby="rcm-tab">
                            <form class="login100-form validate-form flex-sb flex-w" method="post" action="cdata.php">
                                <div style="width: 100%;" class="login100-form validate-form flex-sb flex-w">
                                    <div class="row">

                                            <div class="col-md-5">
                                                <div class="p-t-31 p-b-9">
                                          <span class="txt1">
                                          NPI Number
                                          </span>
                                                </div>
                                                <div class="wrap-input100 validate-input">
                                                    <input class="input100" type="text" placeholder="Npi Number" id="npi" name="npi" value="1609979822">
                                                    <span class="focus-input100"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="p-t-31 p-b-9">
                                          <span class="txt1">
                                          Year
                                          </span>
                                                </div>
                                                <div class="wrap-input100 validate-input">
                                                    <input class="input100" type="text" placeholder="Npi Number" id="year" name="year" value="2019" >
                                                    <span class="focus-input100"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="p-t-31 p-b-9" for="">&nbsp; </label>
                                                <!--                                            <button class="login100-form-btn"  id="test" onclick="testFunction()" >-->
                                                <button class="login100-form-btn"  id="test" type="submit" >
                                                    Get Record
                                                </button>
                                            </div>


                                        <div class="col-md-12 mt-4">
                                            <div class="user-table">
                                                <table class="table table-hover mb-0">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">First Name</th>
                                                        <th scope="col">Last Name</th>
                                                        <th scope="col">First Approval Date</th>
                                                        <th scope="col">Total Nodes</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="myTable">
                                                    <tr>
                                                        <td id="fname"></td>
                                                        <td id="lname"></td>
                                                        <td id="fADate"></td>
                                                        <td id="iLVolume"></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="col" colspan="2">Is Low Volume</th>
                                                        <th scope="col" colspan="2">Organization Name</th>
                                                    </tr>


                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="dropDownSelect1"></div>
<!--===============================================================================================-->
<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/bootstrap/js/popper.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/daterangepicker/moment.min.js"></script>
<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="js/main.js"></script>

<script>


    function testFunction() {
        var npi = $('#npi').val();
        var year = $('#year').val();
        event.preventDefault();

        $.ajax({
            method: "GET",
            url: "cdata.php",
            type : "json",
            data : {'npi' : npi,'year' : year},
            success: function(data){
                console.log(data);
                var obj = JSON.parse(data);
                $('#fname').html(obj.data.clinician.first_name);
                $('#lname').html(obj.data.clinician.last_name);
                $('#fADate').html(obj.data.clinician.first_approval_date);
                $('#iLVolume').html(obj.data.clinician.practiceClinicianStatuses.nodes.length);
//                console.log(obj.data.clinician.practiceClinicianStatuses.nodes[0]['is_low_volume'])
                var nototal = obj.data.clinician.practiceClinicianStatuses.nodes.length;
                for( i = 0; i < nototal; i ++){
                    var table = document.getElementById( 'myTable' ),
                        row = table.insertRow(2),
                        cell1 = row.insertCell(0),
                        cell2 = row.insertCell(1);

                    cell1.innerHTML = obj.data.clinician.practiceClinicianStatuses.nodes[i]['is_low_volume'].toString() ;



                    cell2.innerHTML = obj.data.clinician.practiceClinicianStatuses.nodes[i]['practice'].org_name ;
             }

                console.log(obj.data.clinician.practiceClinicianStatuses.nodes[0]['practice']['practiceStatuses']);


            }

        })

    }

</script>
</body>
</html>